#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 12:51:31 2018

@author: Alexander Knebe
"""

import ahf               as ahf
import numpy             as np
import MyLibrary         as MyL

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

import os
from os import listdir
from os.path import isfile, join

import random
import glob
import re

import scipy             as scp
from scipy.signal import find_peaks
from scipy.signal import peak_widths
from scipy import stats

from skspatial.objects import Points, Plane
from skspatial.plotting import plot_3d

#==============================================================================
# create a structure that holds all relevant constants
#==============================================================================
def def_constants():
    Cdesc = [
        ('datapath'          ,  np.dtype(object)),
        ('PDFdir'            ,  np.dtype(object)),
        ('SIMfile'           ,  np.dtype(object)),
        ('SIMtype'           ,  np.dtype(object)),
        ('obsfile'           ,  np.dtype(object)),
        ('read_ascii_halos'  ,  np.int32),
        ('save_as_npy'       ,  np.int32),
        ('LowerPercentile'   ,  np.float32),
        ('Rsphere'           ,  np.float32),
        ('MhaloLimit_obs'    ,  np.float32),
        ('Npeaks'            ,  np.int32),
        ('f_obs'             ,  np.float32),
        ('f_width'           ,  np.float32),
        ('f_height'          ,  np.float32),
        ('hubble'            ,  np.float32),
        ('BoxSize'           ,  np.float32),
        ('Nbins'             ,  np.int32),
        ('d_virgo_observed'  ,  np.float32),
        ('d_virgo_normalisation'  ,  np.float32),
        ('XLG'               ,  np.float32),
        ('YLG'               ,  np.float32),
        ('ZLG'               ,  np.float32),
        ('RLG'               ,  np.float32),
        ('verbosity'         ,  np.int32)
        ]
    names   = [Cdesc[i][0] for i in range(len(Cdesc))]
    formats = [Cdesc[i][1] for i in range(len(Cdesc))]
    C = np.dtype({'names':names, 'formats':formats}, align=True)
    
    return C

#==============================================================================
# read file with information about which simulations to use
#==============================================================================
def Read_SIMfile(C):
    
    datapath = C['datapath'][0]
    SIMfile  = C['SIMfile'][0]
    
    
    if(len(SIMfile) > 0):
    
        # check for the type of file (e.g. LG, VG, ...)
        pos0    = SIMfile.find('-')
        pos1    = SIMfile.find('.')
        SIMtype = SIMfile[pos0+1:pos1]
        
        # read the file
        data = np.loadtxt(datapath+SIMfile,dtype=np.float)
        
        # extract distance to virgo in Mpc
        if(SIMfile.find('-old.txt')>=0):
            x_c     = np.ones(len(data))*50/C['hubble'][0]
            y_c     = np.ones(len(data))*50/C['hubble'][0]
            z_c     = np.ones(len(data))*50/C['hubble'][0]
            d_virgo = data[:,4]/1000.
        else:
            x_virgo = data[:,2]
            y_virgo = data[:,3]
            z_virgo = data[:,4]
            if(SIMfile.find('VG')>=0):
                x_c      = data[:,5]
                y_c      = data[:,6]
                z_c      = data[:,7]
                d_virgo  = np.sqrt((x_virgo-x_c)**2+(y_virgo-y_c)**2+(z_virgo-z_c)**2)

                # centre of the LG in sheet-coordinate system (cf. caption of Table 1 in McCall et al. 2014)
                C['XLG'] =  0.124  # Mpc
                C['YLG'] = -0.297  # Mpc, puts observer at centre of LG
                C['ZLG'] =  0.200  # Mpc
                
            else:
                # x_c     = data[:,5]
                # y_c     = data[:,6]  # centre of LG
                # z_c     = data[:,7]
                x_c      = data[:,8]
                y_c      = data[:,9]  # MW
                z_c      = data[:,10]
                d_virgo  = np.sqrt((x_virgo-x_c)**2+(y_virgo-y_c)**2+(z_virgo-z_c)**2)

                # centre of the MW in sheet-coordinate system (cf. caption of Table 1 in McCall et al. 2014)
                C['XLG'] =  0.0  # Mpc
                C['YLG'] =  0.0  # Mpc, puts observer onto MW
                C['ZLG'] =  0.0  # Mpc
            
        print('median distance to simulated Virgo:')
        print(np.median(d_virgo),'  (percentiles=',np.percentile(d_virgo,C['LowerPercentile'][0],axis=0),np.percentile(d_virgo,100-C['LowerPercentile'][0],axis=0),')')
        print('')
        
        if(C['d_virgo_normalisation'][0]==0):
            # this measures everything again in absolute distance not normalized by d_virgo...
            d_virgo = np.ones(len(d_virgo))*1.0
        
        # extract virgo masses
        # if(SIMfile.find('VG.txt')>=0):
        #     m_virgo = data[:,3]
        # else:
        #     m_virgo = data[:,0] # this is not correct: TO BE PROVIDED!!!

        # print('median mass of simulated Virgo:')
        # print(np.median(m_virgo),'  (precentiles=',np.percentile(m_virgo,C['LowerPercentile'][0],axis=0),np.percentile(m_virgo,100-C['LowerPercentile'][0],axis=0),')')
        # print('')
              
        # deal with simulation tags
        x = np.array(data, dtype='int32')
        
        # extract only the first two columns and convert them to formatted strings
        N = len(x[:,0])
        a = ['' for x in range(N)]
        b = ['' for x in range(N)]
        for i in range(N):
            a[i] = '{:02d}'.format(x[i,0])
            b[i] = '{:02d}'.format(x[i,1])
        
        # store information in a single string-array
        SIMs = ['' for x in range(N)]
        for i in range(len(a)):
            SIMs[i] = a[i]+'_'+b[i]+'.' # we need the trailing '.' to avoid misindentification with '5(12_XX)_XX'!
    else:
        SIMtype = 'FB'
        SIMs    = []
        
    C['SIMtype']  = SIMtype
    
    LGcentre = {'X':   x_c,
                'Y':   y_c,
                'Z':   z_c}
            
    return SIMs, LGcentre, d_virgo

#==============================================================================
# check for available files in datapath
#==============================================================================
def Scan_datapath(datapath, read_ascii_halos):
    
    if(read_ascii_halos):
        halosfiles = sorted(glob.glob(datapath+'/AHF/HESTIA*_halos'), reverse=True)
    else:
        halosfiles = sorted(glob.glob(datapath+'/AHF.npy/HESTIA*_halos.npy'), reverse=True)
        
            
    return halosfiles

#==============================================================================
# stack multiple halos files
#==============================================================================
def Stack_halos(halos, field):
    data = [halos[i][field] for i in range(len(halos))]
    data = np.array([i for file in data for i in file])
    
    return(data)
        
#==============================================================================
# Select_halos():
#        filter out haloes within C['Rsphere']
#        remove haloes with M < f_obs*MhaloLimit_obs
#        remove haloes with fMhires < 0.98
#==============================================================================
def Select_halos(halos, C):
    
    # careful: we are *not* checking for periodic boundary conditions
    if('Xhalo' in halos.dtype.names):
        dx         = halos['Xhalo']-C['BoxSize'][0]/2
        dy         = halos['Yhalo']-C['BoxSize'][0]/2
        dz         = halos['Zhalo']-C['BoxSize'][0]/2
        good_halos = np.where(dx**2+dy**2+dz**2<C['Rsphere'][0]**2)[0]
        halos      = halos[good_halos]
    
    # mass cut
    if('Mhalo' in halos.dtype.names):
        good_halos = np.where(halos['Mhalo'] >= C['f_obs'][0]*C['MhaloLimit_obs'][0])[0]
        halos      = halos[good_halos]
    
    # fMhires
    if('Mhalo' in halos.dtype.names):
        good_halos = np.where(halos['fMhires'] > 0.999999)[0]
        halos      = halos[good_halos]
    
    if(C['verbosity']>1):    
        if(len(halos)==0):
            print('SelectHalos: zero haloes selected!?')
        
    return halos

#==============================================================================
# Select_Council(): filter out the Council galaxies
#==============================================================================
def Select_Council(obsdata,C):
    # select objects inside C['RLG'] (which usually is set to be 'Rsphere'!) 
    Dist_obs  = np.sqrt((obsdata[:,2]-C['XLG'][0])**2+(obsdata[:,3]-C['YLG'][0])**2+(obsdata[:,4]-C['ZLG'][0])**2)
    pos_good  = np.where(Dist_obs < C['RLG'][0])[0]
    obsdata   = obsdata[pos_good,:]
    
    # select objects with Mhalo >= MhaloLimit_obs
    pos_good = np.where(obsdata[:,1] >= np.log10(C['MhaloLimit_obs']))[0]
    obsdata  = obsdata[pos_good,:]
    
    return obsdata

#==============================================================================
# Convert_units(): eliminate '/h' from all length and mass units (also converting to Mpc)
#==============================================================================
def Convert_units(halos, C):
    
    if('Mhalo' in halos.dtype.names):      halos['Mhalo']      = halos['Mhalo']     /C['hubble'][0]
    if('Xhalo' in halos.dtype.names):      halos['Xhalo']      = halos['Xhalo']     /C['hubble'][0]/1000.
    if('Yhalo' in halos.dtype.names):      halos['Yhalo']      = halos['Yhalo']     /C['hubble'][0]/1000.
    if('Zhalo' in halos.dtype.names):      halos['Zhalo']      = halos['Zhalo']     /C['hubble'][0]/1000.
    if('Rhalo' in halos.dtype.names):      halos['Rhalo']      = halos['Rhalo']     /C['hubble'][0]/1000.
    if('r2' in halos.dtype.names):         halos['r2']         = halos['r2']        /C['hubble'][0]/1000.
    if('mbp_offset' in halos.dtype.names): halos['mbp_offset'] = halos['mbp_offset']/C['hubble'][0]/1000.
    if('com_offset' in halos.dtype.names): halos['com_offset'] = halos['com_offset']/C['hubble'][0]/1000.
    
    return halos

#==============================================================================
# Read_halos: load in all simulations
#==============================================================================
def Read_halos(halosfiles, C):

    halos = []
    
    # loop over all simulation files
    #--------------------------------
    for i,ifile in enumerate(halosfiles):
        
        # read AHF_halos file
        if(C['verbosity'][0]>1):
            print('  Reading halo catalogue',ifile)
        halos_file = ahf.Read_AHF_DMonly(ifile,C['read_ascii_halos'][0],C['save_as_npy'][0],fields=['haloid','Mhalo','Xhalo','Yhalo','Zhalo','fMhires'])
        
        # convert to Msun and Mpc
        halos_file = Convert_units(halos_file, C)
        
        # restrict to objects of interest
        halos_file = Select_halos(halos_file, C)
        
        halos.append(halos_file)
       
    return halos

#==============================================================================
# plot number density distribution
#==============================================================================
def Read_observations(C):
    obsdata   = np.loadtxt(C['obsfile'][0])
    obsdata   = Select_Council(obsdata,C)
    Dist      = np.sqrt((obsdata[:,2]-C['XLG'][0])**2+(obsdata[:,3]-C['YLG'][0])**2+(obsdata[:,4]-C['ZLG'][0])**2)/C['d_virgo_observed'][0]
    
    obsdata = {'logMstar':   obsdata[:,0],
               'logMhalo':   obsdata[:,1],
               'X':          obsdata[:,2]-C['XLG'][0],
               'Y':          obsdata[:,3]-C['YLG'][0],
               'Z':          obsdata[:,4]-C['ZLG'][0],
               'Dist':       Dist  }  # in units of distance to observed Virgo
    
    return obsdata

#==============================================================================
# remove peaks that are not statistically significant
# (the code additionally reduces the data to only the first 3 peaks...)
#==============================================================================
def Remove_peaks(peaks, C):

    # we are checking against the observational peak heights:
    #   we only keep peaks above a certain fraction of their observational counterpart!

    # the old arrays
    peak_simuid = peaks['simuid']
    peak_pos    = peaks['peak_pos']
    peak_width  = peaks['peak_width']
    peak_height = peaks['peak_height']

    # the new arrays    
    peak_simuid_new = []
    peak_pos_new    = []
    peak_width_new  = []
    peak_height_new = []
    
    # keep observational peak as the first entry
    peak_simuid_new.append(peak_simuid[0])
    peak_pos_new.append(peak_pos[0])
    peak_width_new.append(peak_width[0])
    peak_height_new.append(peak_height[0])
    
    # the observational peak heights    
    obs_heights = peak_height[0]
    
    # minimal peak heights to be considered
    min_height0 = obs_heights[0] * C['f_height']
    min_height1 = obs_heights[1] * C['f_height']
    min_height2 = obs_heights[2] * C['f_height']
    
    for i in range(len(peak_simuid)-1):
        simuid  = peak_simuid[i+1]

        pos     = peak_pos[i+1]
        heights = peak_height[i+1]
        widths  = peak_width[i+1]
        
        pos_new     = []
        heights_new = []
        widths_new  = []
                        
        if(len(pos)>0):
            if(heights[0]>=min_height0):
                pos_new.append(pos[0])
                widths_new.append(widths[0])
                heights_new.append(heights[0])
                
        if(len(pos)>1):
            if(heights[1]>=min_height1):
                pos_new.append(pos[1])
                widths_new.append(widths[1])
                heights_new.append(heights[1])
                
        if(len(pos)>2):
            if(heights[2]>=min_height2):
                pos_new.append(pos[2])
                widths_new.append(widths[2])
                heights_new.append(heights[2])
                
        if(len(pos_new)>0):
            peak_simuid_new.append(simuid)
            peak_pos_new.append(pos_new)
            peak_width_new.append(widths_new)
            peak_height_new.append(heights_new)
            
    
    peaks_new = {'peak_pos':    peak_pos_new,
                 'peak_height': peak_height_new,
                 'peak_width':  peak_width_new,
                 'simuid':      peak_simuid_new}
    
    peaks = 0
    
    return peaks_new
    
#==============================================================================
# Kernel Density Estimate peak analysis
#==============================================================================
def KDE(halos, LGcentre, d_virgo, C, plot='no'):

    # for Gaussian KDE
    covf      = 0.2   
    pos_Dcut  = 3    # we ignore tge first entries which are MW, M31, and M33     
    
    # prepare plot
    plt.figure()
    if(C['d_virgo_normalisation'][0]==1):
        xlabel = '$D/D_{Virgo}$'
    else:
        xlabel = '$D$ [Mpc]'
    ylabel = 'Gaussian KDE'

    # accumulate peak statistics
    peak_pos    = []
    peak_width  = []
    peak_height = []
    simuid      = []

    # deal with the observational data
    #----------------------------------
    if(C['obsfile'][0] != ''):
        obsdata   = np.loadtxt(C['obsfile'][0])
        obsdata   = Select_Council(obsdata,C)
        Dist_obs  = np.sqrt((obsdata[:,2]-C['XLG'][0])**2+(obsdata[:,3]-C['YLG'][0])**2+(obsdata[:,4]-C['ZLG'][0])**2)/C['d_virgo_observed'][0]
        Dist_obs  = np.sort(Dist_obs)[::1]
        
#       actual number density (not of interest though...)
        Dist_obs_mid = ((Dist_obs[0:-1]+Dist_obs[1:])/2)
        dDist_obs    = Dist_obs[1:]-Dist_obs[0:-1]
        NDens_obs   = 1/(Dist_obs_mid**2*dDist_obs)
#        plt.plot(Dist_obs[1:], np.log10(NDens_obs),'ro',markersize=5,label='McGill et al. (2014)')
#        plt.plot(Dist_obs[1:], np.log10(NDens_obs),'r')

        # Gaussian Kernel Density
        [x,d] = MyL.Gaussian_KDE(Dist_obs[pos_Dcut:], covf)
        Delta_x=(np.diff(x))[0]
        if(plot == 'yes'):
            plt.plot(x, d(x),'r',label='McGill et al. (2014)')

        # get peak statistics
        p_pos    = find_peaks(d(x))
        p_width  = peak_widths(d(x), p_pos[0])
        p_height = d(x[np.array(p_pos[0])])

        # accumulate statistics (first entry will be observational data!)
        peak_pos.append(x[p_pos[0]])
        peak_width.append(p_width[0]*Delta_x) # note, peak_widths() return width as 'number of elements'
        peak_height.append(p_height)
        simuid.append(-1)

    # loop over all simulation files
    #--------------------------------
    for i in range(len(halos)):
                
        halos_file = halos[i]
        
        # careful: we are *not* checking for periodic boundary conditions
        dx = halos_file['Xhalo']-LGcentre['X'][i]
        dy = halos_file['Yhalo']-LGcentre['Y'][i]
        dz = halos_file['Zhalo']-LGcentre['Y'][i]
        
        # construct and plot individual data set
        Dist_file  = np.sqrt(dx**2+dy**2+dz**2)/d_virgo[i]
        Dist_file  = np.sort(Dist_file)[::1]
#        print(Dist_file)

#       actual number density (not of interest though...)
#        Dist_file_mid = ((Dist_file[0:-1]+Dist_file[1:])/2)
#        dDist_file    = Dist_file[1:]-Dist_file[0:-1]
#        NDens_file    = 1/(Dist_file_mid**2*dDist_file)
#        plt.plot(Dist_file[1:], np.log10(NDens_file),linewidth=0.1,alpha=0.2)

        # Gaussian Kernel Density
        covf      = 0.2        
        [x,d] = MyL.Gaussian_KDE(Dist_file[pos_Dcut:], covf)
        Delta_x=(np.diff(x))[0]
        if(plot=='yes'):
            plt.plot(x, d(x),linewidth=0.1,alpha=0.8)
          
        # get peak statistics
        p_pos    = find_peaks(d(x))
        p_width  = peak_widths(d(x), p_pos[0])
        p_height = d(x[np.array(p_pos[0])])
        
        # accumulate statistics
        peak_pos.append(x[p_pos[0]])
        peak_width.append(p_width[0]*Delta_x) # note, peak_widths() return width as 'number of elements'
        peak_height.append(p_height)
        simuid.append(i)
        
    # plot, if wanted
    #-----------------
    if(plot=='yes'):
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend(loc='upper left')
            
        # save figure
        outfile_name = C['PDFdir'][0]+'KDE'+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
        plt.savefig(outfile_name)
           
#    peak_pos    = MyL.Stack_data(peak_pos)
#    peak_width  = MyL.Stack_data(peak_width)
#    peak_height = MyL.Stack_data(peak_height)
    
    returndata = {'peak_pos':    peak_pos,
                  'peak_width':  peak_width,
                  'peak_height': peak_height,
                  'simuid':      simuid}
    
    return returndata

#==============================================================================
# plot statistics of peak widths
#==============================================================================
def Plot_peakwidth(peaks, C):
 
    Npeaks      = C['Npeaks'][0]  # how many peaks to eventually plot

    peak_pos    = peaks['peak_pos']
    peak_width  = peaks['peak_width']
    peak_height = peaks['peak_height']
    
    # extract the first 3 simulation peaks
    widths01 = []
    widths02 = []
    widths03 = []
    for i in range(len(peak_width)-1):
        peaks   = peak_pos[i+1]
        widths  = peak_width[i+1]
        if(len(peaks)>0):
            widths01.append(widths[0])
        if(len(peaks)>1):
            widths02.append(widths[1])
        if(len(peaks)>2):
            widths03.append(widths[2])

    # extract the first 3 observed peaks
    obs_widths  = peak_width[0]

    # store in more flexible arrays
    labels      = ['1st simulation peak','2nd simulation peak','3rd simulation peak']
    colors      = ['b','g','r']
    alphas      = [1, 0.75,0.5]
    widths      = [np.array(widths01), np.array(widths02), np.array(widths03)]
    olabels     = ['1st observed peak','2nd observed peak','3rd observed peak']
    obs_heights = [obs_widths[0], obs_widths[1], obs_widths[2]]
    
    Nbins = 10
    maxN = -1

    # plot simulation peaks
    plt.figure(12)    
    for i in range(Npeaks):        
        hist = plt.hist(widths[i],color=colors[i],alpha=alphas[i],label=labels[i],bins=Nbins,histtype='barstacked')        
        if(max(hist[0])>maxN):
            maxN = max(hist[0])
    
    # plot observed peaks
    for i in range(Npeaks):
        plt.plot([obs_widths[i],obs_widths[i]],[0,maxN],color=colors[i],linewidth=4,label=olabels[i])

    if(C['d_virgo_normalisation'][0]==0):
        plt.xlabel('KDE peak width [Mpc]')
    else:
        plt.xlabel('KDE peak width')
        
    plt.ylabel('N')
    plt.legend()
    plt.title('peak width')
#    plt.xlim([0,1])
    plt.ylim([0,maxN])
#    plt.ylim([0,240])

    outfile_name = C['PDFdir'][0]+'peak_width'+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)
    
#==============================================================================
# plot statistics of peak heights
#==============================================================================
def Plot_peakheight(peaks, C):
    
    Npeaks      = C['Npeaks'][0]  # how many peaks to eventually plot
    
    peak_pos    = peaks['peak_pos']
    peak_width  = peaks['peak_width']
    peak_height = peaks['peak_height']
    
    # extract the first 3 simulation peaks
    heights01 = []
    heights02 = []
    heights03 = []
    for i in range(len(peak_height)-1):
        peaks   = peak_pos[i+1]
        heights = peak_height[i+1]
        if(len(peaks)>0):
            heights01.append(heights[0])
        if(len(peaks)>1):
            heights02.append(heights[1])
        if(len(peaks)>2):
            heights03.append(heights[2])

    # extract the first 3 observed peaks
    obs_heights = peak_height[0]

    # store in more flexible arrays
    labels      = ['1st simulation peak','2nd simulation peak','3rd simulation peak']
    colors      = ['b','g','r']
    alphas      = [1, 0.75,0.5]
    heights     = [np.array(heights01), np.array(heights02), np.array(heights03)]
    olabels     = ['1st observed peak','2nd observed peak','3rd observed peak']
    obs_heights = [obs_heights[0], obs_heights[1], obs_heights[2]]
    llabels     = ['1st height limit','2nd height limit','3rd height limit']
    
    Nbins = 10
    maxN = -1

    # plot simulation peaks
    plt.figure(10)    
    for i in range(Npeaks):        
        hist = plt.hist(heights[i],color=colors[i],alpha=alphas[i],label=labels[i],bins=Nbins,histtype='barstacked')        
        if(max(hist[0])>maxN):
            maxN = max(hist[0])
    
    # plot observed peaks
    for i in range(Npeaks):
        plt.plot([obs_heights[i],obs_heights[i]],[0,maxN],color=colors[i],linewidth=4,label=olabels[i])

    # plot limits
    for i in range(Npeaks):
        plt.plot(np.array([obs_heights[i],obs_heights[i]])*C['f_height'][0],[0,maxN],color=colors[i],linewidth=2,linestyle='--',label=llabels[i])

    plt.xlabel('KDE peak height')
    plt.ylabel('N')
    plt.legend()
    plt.title('peak height')
#    plt.xlim([0,1])
    plt.ylim([0,maxN])
#    plt.ylim([0,240])

    outfile_name = C['PDFdir'][0]+'peak_height'+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)
    
#==============================================================================
# plot statistics of peak positions
#==============================================================================
def Plot_peakpos(peaks, C):
    
    Npeaks      = C['Npeaks'][0]  # how many peaks to eventually plot
    
    if(C['d_virgo_normalisation'][0]==1):
        xlabel = '$D/D_{Virgo}$'
    else:
        xlabel = '$D$ [Mpc]'
    ylabel = 'N'
    
    peak_pos    = peaks['peak_pos']
    peak_width  = peaks['peak_width']
    peak_height = peaks['peak_height']
    
    # extract the first 3 observed peak heights
    obs_heights = peak_height[0]

    # extract the first 3 simulation peaks
    pos01 = []
    pos02 = []
    pos03 = []
    for i in range(len(peak_pos)-1):
        pos     = peak_pos[i+1]
        heights = peak_height[i+1]
        if(len(pos)>0):
            pos01.append(pos[0])
        if(len(pos)>1):
            pos02.append(pos[1])
        if(len(pos)>2):
            pos03.append(pos[2])

    # extract the first 3 observed peaks
    obs_pos = peak_pos[0]

    # store in more flexible arrays
    labels  = ['1st simulation peak','2nd simulation peak','3rd simulation peak']
    colors  = ['b','g','r']
    alphas  = [1, 0.75,0.5]
    pos     = [np.array(pos01), np.array(pos02), np.array(pos03)]
    obs_pos = [obs_pos[0], obs_pos[1], obs_pos[2]]
    
    Nbins = 7
    maxN = -1

    # plot simulation peaks
    plt.figure(11)    
    for i in range(Npeaks):        
        hist = plt.hist(pos[i],label=labels[i],color=colors[i],alpha=alphas[i],bins=Nbins,histtype='barstacked')        
        if(max(hist[0])>maxN):
            maxN = max(hist[0])
    
    # plot observed peaks
    for i in range(Npeaks):
        if(i==0):
            plt.plot([obs_pos[i],obs_pos[i]],[0,maxN],'r-',linewidth=4,label='observed peaks')
        else:
            plt.plot([obs_pos[i],obs_pos[i]],[0,maxN],'r-',linewidth=4)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.title('peak positions')
    # plt.xlim([0,1])
    plt.ylim([0,maxN])
#    plt.ylim([0,240])

    outfile_name = C['PDFdir'][0]+'peak_pos'+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)
    
#==============================================================================
# Extract_peakhalos: find haloids that comply with certain peak constraints
#==============================================================================
def Extract_peakhalos(halos, peaks, peakid, LGcentre, d_virgo, C):
    
    # note,         peaks[] contains only simulation with significan peaks (and as entry [0] the observations!)
    #       whereas halos[] still contains *all* simulations (same is true for LGcentre[])!
    Nsimu     = len(peaks['simuid'])-1
        
    # obs_peak[i][j]:
    #      i=0,1,2 for pos, height, width;
    #      j=0,1,2,... for peakid
    obs_peak    = [peaks['peak_pos'][0],peaks['peak_height'][0],peaks['peak_width'][0]]
    
    # sim_peaks[i][k][j]:
    #      i=0,1,2 for pos, height, width;
    #      k=0,1,2,... for simuid
    #      j=0,1,2,... for peakid
    sim_peaks   = [peaks['peak_pos'][1:],peaks['peak_height'][1:],peaks['peak_width'][1:]]  

    peaksimuid     = []
    peakhaloids    = []
    peakhalos      = []
    peakhalodist   = []
    peakcofm       = []
    peakcofmdist   = []
     
    # loop over all simulation files
    #--------------------------------
    for i in range(Nsimu):
                              
        simuid = peaks['simuid'][i+1] # note, peaks[][0] is the observational data
        
        # select the halos of simulation simuid
        halos_file = halos[simuid]
                    
        # careful: we are *not* checking for periodic boundary conditions
        dx = halos_file['Xhalo']-LGcentre['X'][simuid]
        dy = halos_file['Yhalo']-LGcentre['Y'][simuid]
        dz = halos_file['Zhalo']-LGcentre['Z'][simuid]
        
        # construct distance
        Dist_file  = np.sqrt(dx**2+dy**2+dz**2)/d_virgo[i]
        
        # set distance range according to peakid: peak_pos of peakid +- peak_width/2
        if(len(sim_peaks[0][i])>peakid):
            Dist_min = sim_peaks[0][i][peakid]-sim_peaks[2][i][peakid]/2 * C['f_width'][0]
            Dist_max = sim_peaks[0][i][peakid]+sim_peaks[2][i][peakid]/2 * C['f_width'][0]
        else:
            Dist_min =  1e40
            Dist_max = -1e40
        
        if(C['verbosity'][0]>1):
            print('Extract_peakhalos: searching',simuid,'within Dist=[',Dist_min,',',Dist_max,']')
    
        # find positions in Dist_file[] -> those are our desired peak haloes...
        peakhalos_pos = np.where((Dist_min<Dist_file) & (Dist_file<Dist_max))[0]

        if(C['verbosity'][0]>1):
            print('Extract_peakhalos: found',len(peakhalos_pos),' peak halos')
            print('')

        # only makes sense to store if actually found (that's why we need to also store simuid==i)
        if(len(peakhalos_pos)>0):
            peakhalos_file    = halos_file[peakhalos_pos]

            # center of mass calculation
            peakhalos_mass = peakhalos_file['Mhalo']
            print("SIM PEAK HALOS MASSES")
            print(peakhalos_mass)
            peakhalos_mass_tot = sum(peakhalos_file['Mhalo'])

            # peak halo coordinates
            peakdx = dx[peakhalos_pos]
            peakdy = dy[peakhalos_pos]
            peakdz = dz[peakhalos_pos]

            cofmx = peakdx * peakhalos_mass
            cofmy = peakdy * peakhalos_mass
            cofmz = peakdz * peakhalos_mass
            #print()
            #print("PEAK POSITIONS SIM")
            #print(peakdx)
            #print(peakdy)
            #print(peakdz)

            # accumulate peaks into array
            peakcofm.append([np.sum(cofmx), np.sum(cofmy), np.sum(cofmz)] / peakhalos_mass_tot)
            # distances between C of M and LG center
            peakcofmdist.append(
                np.sqrt(peakcofm[-1][0]**2 + peakcofm[-1][1]**2 + peakcofm[-1][2]**2))
            peakhaloids_file  = peakhalos_file['haloid']
            peakhalodist_file = Dist_file[peakhalos_pos]

            # append all information to final arrays                
            peaksimuid.append(simuid)
            peakhaloids.append(peakhaloids_file)
            peakhalos.append(peakhalos_file)
            peakhalodist.append(peakhalodist_file)

            
            
        else:
            if(C['verbosity'][0]>1):
                print('Extract_peakhalos: no peak halos for simuid=',simuid)
        
    print("SIM C OF M dist")
    print(peakcofmdist)
    returndata = {'peaksimuid':   peaksimuid,       # we need to keep track of the simuids for which there were significant/uesable peaks
                  'peakhaloids':  peakhaloids,      # these are just the haloids of the halos within the peak
                  'peakhalos':    peakhalos,        # this is the full halo catalogue
                  'peakhalodist': peakhalodist,
                  'peakcofm':     peakcofm,
                  'peakcofmdist': peakcofmdist}     # we also keep track of the distance *in units of d_virgo*!
            
    return returndata

#==============================================================================
# Extract_peakobs: find observed galaxies that lie define peakid
#==============================================================================
def Extract_peakobs(obsdata, peaks, peakid, C):
            
    # obs_peak[i][j]:
    #      i=0,1,2 for pos, height, width;
    #      j=0,1,2,... for peakid
    obs_peak    = [peaks['peak_pos'][0],peaks['peak_height'][0],peaks['peak_width'][0]]
    
    Dist_obs = obsdata['Dist']
    
    # set distance range according to peakid: peak_pos of peakid +- peak_width/2
    Dist_min = obs_peak[0][peakid]-obs_peak[2][peakid]/2 * C['f_width'][0]
    Dist_max = obs_peak[0][peakid]+obs_peak[2][peakid]/2 * C['f_width'][0]
    
    if(C['verbosity'][0]>1):
        print('Find_peakobs: searching within Dist=[',Dist_min,',',Dist_max,']')
    print("DIST OBS")
    print(Dist_obs)
    # find positions in Dist_file[] -> those are our desired peak galaxies...
    peakobs_pos = np.where((Dist_min<Dist_obs) & (Dist_obs<Dist_max))[0]

    # center of mass calculation
    peakhalos_mass_tot = sum(10**obsdata['logMhalo'][peakobs_pos])
    print(obsdata['logMhalo'][peakobs_pos])
    print("OBS PEAK TOT MASS")
    print(peakhalos_mass_tot)
    cofmx = obsdata['X'][peakobs_pos] * 10**obsdata['logMhalo'][peakobs_pos]
    print(cofmx)
    cofmy = obsdata['Y'][peakobs_pos] * 10**obsdata['logMhalo'][peakobs_pos]
    cofmz = obsdata['Z'][peakobs_pos] * 10**obsdata['logMhalo'][peakobs_pos]
    print("OBS X")
    print(obsdata['X'][peakobs_pos])
    print("OBS Y")
    print(obsdata['Y'][peakobs_pos])
    print("OBS Z")
    print(obsdata['Z'][peakobs_pos])
    #print("PEAK POSITIONS OBS")
    #print(obsdata['X'][peakobs_pos])
    #print(obsdata['Y'][peakobs_pos])
    #print(obsdata['Z'][peakobs_pos])
    peakcofm = ([np.sum(cofmx), np.sum(cofmy), np.sum(cofmz)] / peakhalos_mass_tot)
    # distances between C of M and LG center
    peakcofmdist = np.sqrt(peakcofm[0]**2 + peakcofm[1]**2 + peakcofm[2]**2)
        
    obsdata_new = {'logMstar':     obsdata['logMstar'][peakobs_pos],
                   'logMhalo':     obsdata['logMhalo'][peakobs_pos],
                   'X':            obsdata['X'][peakobs_pos],
                   'Y':            obsdata['Y'][peakobs_pos],
                   'Z':            obsdata['Z'][peakobs_pos],
                   'Dist':         obsdata['Dist'][peakobs_pos],
                   'peakcofm':     peakcofm,
                   'peakcofmdist': peakcofmdist}
    print()
    print("OBS C OF M dist")
    print(peakcofmdist)
                     
    return obsdata_new

#==============================================================================
# Plot_numbers: plot the number of objects per peak as a function of simuid
#==============================================================================
def Plot_numbers(halos, obs, peakid, C):

    Nsimu = len(halos['peaksimuid'])

    simuid    = []
    Nhalo     = []
    Nhalo_obs = []

    for i in range(Nsimu):
        peakhalos_i = halos['peakhalos'][i]
        Nhalo_i  = len(peakhalos_i['Mhalo'])
        
        Nhalo.append(Nhalo_i)
        
        Nhalo_obs.append(len(obs['logMhalo']))
        
        simuid.append(halos['peaksimuid'][i])
        

    Nhalo     = np.array(Nhalo)
    Nhalo_obs = np.array(Nhalo_obs)
    
    plt.figure(30)
    plt.plot(simuid,Nhalo,label='peak halos',linewidth=0.5)
    plt.plot(simuid,Nhalo_obs,label='observation')
    plt.ylabel('$N$')
    plt.xlabel('simulation')
    title = 'number of of objects in peak #'+str(peakid+1)
    plt.title(title)
    xmin = min(halos['peaksimuid'])
    xmax = max(halos['peaksimuid'])+1
    plt.xlim([xmin,xmax])
    plt.legend(loc='upper right')

    # annotate useful information
    Ntotal = len(simuid)
    Ngood  = len(np.where(Nhalo>=Nhalo_obs)[0])
    s=f'$N\geq N^{{obs}}$: {Ngood/Ntotal*100:3.0f}%'
    plt.annotate(s, xy=(0.05, 0.95), xycoords='axes fraction')
    medNhalo     = np.median(Nhalo)
    s=f'med(log$N$) = {medNhalo:4.2f}, med(log$N^{{obs}}$ = {Nhalo_obs[0]:4.2f})'
    plt.annotate(s, xy=(0.05, 0.88), xycoords='axes fraction')

    outfile_name = C['PDFdir'][0]+'numbers-peakid='+str(peakid)+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)
        
    returndata = {'simuid': simuid,
                  'Nhalo':  Nhalo,
                  'Nobs':   Nhalo_obs}
            

    return returndata

#==============================================================================
# Plot_masses: plot the median masses+-percentiles as a function of simuid
#==============================================================================
def Plot_masses(halos, obs, peakid, C):

    Nsimu = len(halos['peaksimuid'])

    medlogMhalo_obs      = np.median(obs['logMhalo'])
    medlogMhalo_obs_low  = np.percentile(medlogMhalo_obs,C['LowerPercentile'][0])
    medlogMhalo_obs_high = np.percentile(medlogMhalo_obs,100-C['LowerPercentile'][0])  


    simuid = []

    logMhalo      = []
    logMhalo_low  = []
    logMhalo_high = []
    
    logMhalo_obs      = []
    logMhalo_obs_low  = []
    logMhalo_obs_high = []

    for i in range(Nsimu):
        # simulation
        #-----------
        # get halos within peak
        peakhalos_i = halos['peakhalos'][i]
        
        # get all their masses
        logMhalo_i  = np.log10(peakhalos_i['Mhalo'])
        
        # accumulate median+-percentiles of masses
        logMhalo.append(np.median(logMhalo_i))
        logMhalo_low.append(np.percentile(logMhalo_i,C['LowerPercentile'][0]))
        logMhalo_high.append(np.percentile(logMhalo_i,100-C['LowerPercentile'][0]))
        
        # observation
        #-------------
        logMhalo_obs.append(medlogMhalo_obs)
        logMhalo_obs_low.append(medlogMhalo_obs_low)
        logMhalo_obs_high.append(medlogMhalo_obs_high)
        
        # keep track of which simulation was used...
        #--------------------------------------------
        simuid.append(halos['peaksimuid'][i])
        
    # convert to numpy arrays
    logMhalo      = np.array(logMhalo)
    logMhalo_low  = np.array(logMhalo_low)
    logMhalo_high = np.array(logMhalo_high)
    logMhalo_obs      = np.array(logMhalo_obs)
    logMhalo_obs_low  = np.array(logMhalo_obs_low)
    logMhalo_obs_high = np.array(logMhalo_obs_high)
    
    plt.figure(50)
    plt.errorbar(simuid,logMhalo,yerr=[logMhalo-logMhalo_low,logMhalo_high-logMhalo],label='peak halos',elinewidth=0.5)
    plt.plot(simuid,logMhalo_obs,label='observation')
    plt.ylabel('$log(M_{halo}[Msun])$')
    plt.xlabel('simulation')
    title = 'median mass of objects in peak #'+str(peakid+1)
    plt.title(title)
    xmin = min(halos['peaksimuid'])
    xmax = max(halos['peaksimuid'])+1
    plt.xlim([xmin,xmax])
    plt.legend(loc='upper right')

    # annotate useful information
    Ntotal = len(simuid)
    Ngood  = len(np.where(logMhalo>=logMhalo_obs)[0])
    s=f'$M\geq M^{{obs}}$: {Ngood/Ntotal*100:3.0f}%'
    plt.annotate(s, xy=(0.05, 0.95), xycoords='axes fraction')
    medlogMhalo     = np.median(logMhalo)
    medlogMhalo_obs = np.median(logMhalo_obs)
    s=f'med(log$M$) = {medlogMhalo:4.2f}, med(log$M^{{obs}}$ = {medlogMhalo_obs:4.2f})'
    plt.annotate(s, xy=(0.05, 0.88), xycoords='axes fraction')

    outfile_name = C['PDFdir'][0]+'masses-peakid='+str(peakid)+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)
            
    returndata = {'simuid':            simuid,
                  'logMhalo':          logMhalo,
                  'logMhalo_obs':      medlogMhalo_obs,
                  'logMhalo_obs_low':  medlogMhalo_obs_low,
                  'logMhalo_obs_high': medlogMhalo_obs_high }
            
    return returndata

#==============================================================================
# Plot_planefit: fit plane to data and plot fit quality ('chi distance')
#==============================================================================
def Dist_Plane_Points(plane,points):
    Dist = []
    for i in range(len(points)):    
        Dist.append(np.sqrt(plane.distance_point_signed(points[i])**2))
    return np.mean(Dist)

def Plot_planefit(halos, obs, peakid, LGcentre, C):
    
    # get observational plane
    x_obs   = np.array(obs['X']-C['XLG'][0])
    y_obs   = np.array(obs['Y']-C['YLG'][0])
    z_obs   = np.array(obs['Z']-C['ZLG'][0])
    xyz_obs = np.transpose(np.array([x_obs,y_obs,z_obs]))
    Nobs    = len(x_obs)
         
    plane   = Plane.best_fit(Points(xyz_obs))
    d_obs_i = Dist_Plane_Points(plane,xyz_obs)          
            
    # arrays to be filled for each simulation
    Nsimu      = len(halos['peaksimuid'])
    simuid     = []
    d          = []
    d_obs      = []

    for i in range(Nsimu):

        peakhalos_i = halos['peakhalos'][i]
        x_i = np.array(peakhalos_i['Xhalo']-LGcentre['X'][halos['peaksimuid'][i]])
        y_i = np.array(peakhalos_i['Yhalo']-LGcentre['Y'][halos['peaksimuid'][i]])
        z_i = np.array(peakhalos_i['Zhalo']-LGcentre['Z'][halos['peaksimuid'][i]])

        # fitting an ellipsoid only makes sense when there are enough points
        if(len(x_i) >= int(C['f_obs'][0]*Nobs)):
            
            xyz_i   = np.transpose(np.array([x_i,y_i,z_i]))
            plane_i = Plane.best_fit(Points(xyz_i))
            d_i     = Dist_Plane_Points(plane_i,xyz_i)          

            # only record the relevant simulations
            if(np.median(d_i)<1000*d_obs_i):
                d.append(np.median(d_i))
    
                # the observational data is always the same (and therefore calculated outside the for-loop)
                d_obs.append(d_obs_i)
                    
                # keep track of which simulation was used...
                simuid.append(halos['peaksimuid'][i])
                        
    d     = np.array(d)
    d_obs = np.array(d_obs)
    
    plt.figure(33)
    plt.plot(simuid,d,label='peak halos',linewidth=0.5)
    plt.plot(simuid,d_obs,label='observation')
    plt.ylabel('$<d_{\perp}>$ [Mpc]')
    plt.xlabel('simulation')
    title = 'mean perpendicular distance to best-fit plane of objects in peak #'+str(peakid+1)
    plt.title(title,fontsize='small')
    xmin = min(halos['peaksimuid'])
    xmax = max(halos['peaksimuid'])+1
    plt.xlim([xmin,xmax])
    plt.legend(loc='upper right')
    
    # annotate ueseful information
    Ntotal = len(simuid)
    Ngood  = len(np.where(d<=d_obs)[0])
    s=f'$d_\perp \leq d_\perp^{{obs}}$: {Ngood/Ntotal*100:3.0f}%$'
    plt.annotate(s, xy=(0.05, 0.95), xycoords='axes fraction')
    medd     = np.median(d)
    s=f'med($d_\perp$)={medd:4.2f}Mpc, $d_\perp^{{obs}}$={d_obs[0]:4.2f}Mpc'
    plt.annotate(s, xy=(0.05, 0.88), xycoords='axes fraction')
    Nsim = int(C['f_obs'][0]*Nobs)
    s=f'$N_{{lim}}$={Nsim:2.0f}, $N^{{obs}}$={Nobs:2.0f}'
    plt.annotate(s, xy=(0.05, 0.81), xycoords='axes fraction')
    
    outfile_name = C['PDFdir'][0]+'planes-peakid='+str(peakid)+'-R'+str(C['Rsphere'][0])+'Mpc-'+C['SIMtype'][0]+'.pdf'
    plt.savefig(outfile_name)

    returndata = {'simuid':  simuid,
                  'd':       d,
                  'd_obs':   d_obs_i}
            
    return returndata

#==============================================================================
# Extract_simutag: get the XX_YY part of the filename
#==============================================================================
def Extract_simutag(halosfiles):

    simutag = []
    for i in range(len(halosfiles)):
        pos0 = halosfiles[i].find('512_')
        pos1 = halosfiles[i].find('.127')
        
        simutag.append(halosfiles[i][pos0+4:pos1])
        
    return np.array(simutag)

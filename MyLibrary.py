#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 09:56:00 2018

@author: aknebe
"""
# import all relevant modules
#=================================================
import numpy             as np

import matplotlib.pyplot as plt

import scipy
from scipy import interpolate

from scipy.spatial import ConvexHull, convex_hull_plot_2d
from numpy.linalg  import eig, inv, det


#==============================================================================
# calculate the mid-points
#==============================================================================
def Calc_midpoints (X):

    Y = (X[0:len(X)-1]+X[1:len(X)])/2
    
    return Y

#==============================================================================
# plot the differential distribution of X()>limit data
#==============================================================================
def PlotHistogram (X, norm, limit, Nbins, lstyle, lwidth, xlabel, ylabel, label):
    
    # remove X=0 entries
    igood = np.where(X > limit)[0]
    X     = X[igood]
            	
    # generate histogram
    hist, bin_edges = np.histogram((X),bins=Nbins)
    
    # dlogX
    dX = np.diff(bin_edges)
    
    # mid-points
    bin_X = Calc_midpoints(bin_edges)

    # eventually plot distribution
    plt.plot(bin_X[0:len(hist)-1],(hist[0:len(hist)-1]/dX[0:len(hist)-1]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
#    plt.bar(bin_X[0:len(hist)-1],(hist[0:len(hist)-1]/dX[0:len(hist)-1]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    	
    return    


#==============================================================================
# plot the differential distribution of X()>limit data
#==============================================================================
def PlotYLogHistogram (X, norm, limit, Nbins, lstyle, lwidth, xlabel, ylabel, label):
    
    # remove X=0 entries
    igood = np.where(X > limit)[0]
    X     = X[igood]
            	
    # generate histogram
    hist, bin_edges = np.histogram((X),bins=Nbins)
    
    # dlogX
    dX = np.diff(bin_edges)
    
    # mid-points
    bin_X = Calc_midpoints(bin_edges)

    # filter zeros (as they casue trouble on a log-scale)
    pos_good = np.where(hist != 0)[0]

    # eventually plot distribution
    plt.plot(bin_X[pos_good],np.log10(hist[pos_good]/dX[pos_good]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
#    plt.bar(bin_X[pos_good],np.log10(hist[pos_good]/dX[pos_good]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    	
    return    

#==============================================================================
# plot the differential distribution of X()>limit data
#==============================================================================
def PlotXLogHistogram (X, norm, limit, Nbins, lstyle, lwidth, xlabel, ylabel, label):
    
    # remove X=0 entries
    igood = np.where(X > limit)[0]
    X     = X[igood]
            	
    # generate histogram
    hist, bin_edges = np.histogram((X),bins=Nbins)
    
    # dlogX
    dX = np.diff(bin_edges)
    
    # mid-points
    bin_X = Calc_midpoints(bin_edges)

    # filter zeros (as they casue trouble on a log-scale)
    pos_good = np.where(hist != 0)[0]

    # eventually plot distribution
    plt.plot(np.log10(bin_X[pos_good]),(hist[pos_good]/dX[pos_good]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
#    plt.bar(np.log10(bin_X[pos_good]),(hist[pos_good]/dX[pos_good]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    	
    return    

#==============================================================================
# plot the differential distribution of log10(X()>limit data)
#==============================================================================
def PlotLogHistogram (X, norm, limit, Nbins, lstyle, lwidth, xlabel, ylabel, label):
    
    # filter X>limit entries
    igood = np.where(X > limit)[0]
    X     = X[igood]
            	
    # generate histogram
    hist, bin_edges = np.histogram(np.log10(X),bins=Nbins)
    
    # dlogX
    dlogX = np.diff(bin_edges)
        
    # mid-points
    bin_X = Calc_midpoints(bin_edges)
    
    # filter zeros (as they casue trouble on a log-scale)
    pos_good = np.where(hist != 0)[0]
    
    # eventually plot distribution
    plt.plot(bin_X[pos_good],np.log10(hist[pos_good]/dlogX[pos_good]/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    	
    return    


#==============================================================================
# plot the cumulative distribution of log10(X()>limit) data
#==============================================================================
def PlotLogCumulative (X, norm, limit, lstyle, lwidth, xlabel, ylabel, label):
  
    # filter X>limit entries
    igood = np.nonzero(X > limit)
    X     = X[igood]
    
    #X.sort(reverse=True)   # this only works for lists and not for float arrays...
    X = np.sort(X)
    X = X[::-1]
    N = 1+np.array(range(len(X)))
    plt.plot(np.log10(X),np.log10(N/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    return

#==============================================================================
# plot the cumulative distribution of X()>limit data (log10() of y-axis)
#==============================================================================
def PlotYLogCumulative (X, norm, limit, lstyle, lwidth, xlabel, ylabel, label):
  
    # filter X>limit entries
    igood = np.nonzero(X > limit)
    X     = X[igood]
    
    #X.sort(reverse=True)   # this only works for lists and not for float arrays...
    X = np.sort(X)
    X = X[::-1]
    N = 1+np.array(range(len(X)))
    plt.plot((X),np.log10(N/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    return

#==============================================================================
# plot the cumulative distribution of X()>limit data
#==============================================================================
def PlotCumulative (X, norm, limit, lstyle, lwidth, xlabel, ylabel, label):
  
    # filter X>limit entries
    igood = np.nonzero(X > limit)
    X     = X[igood]
    
    #X.sort(reverse=True)   # this only works for lists and not for float arrays...
    X = np.sort(X)
    X = X[::-1]
    N = 1+np.array(range(len(X)))
    plt.plot((X),(N/norm),linestyle=lstyle,linewidth=lwidth,label=label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    return

#==============================================================================
# bin X() data and plot median(Y())+-Percentiles in each bin
#==============================================================================
def PlotMedian (X, Y, Nbins, Percentile_low, lstyle, lwidth, xlabel, ylabel, label):
  
    Percentile_up  = 100-Percentile_low
    
    # bins the data between min/max
    Xmin  = min(X)
    Xmax  = max(X)
    dX    = (Xmax-Xmin)/(Nbins-1)
    
    # prepare all arrays for the bins
    Xbin     = np.zeros(Nbins)
    Ybin     = np.zeros(Nbins)
    Ybin_low = np.zeros(Nbins)
    Ybin_up  = np.zeros(Nbins)
    Xbin_low = np.zeros(Nbins)
    Xbin_up  = np.zeros(Nbins)
    
    # loop over desired bins
    for i in range(Nbins):
        irange  = np.where( (Xmin+i*dX < X)  &  (X <= Xmin+(i+1)*dX) )[0]
        
        if len(irange) > 0:
            Xbin[i] = np.median(X[irange])
            Ybin[i] = np.median(Y[irange])
            
            # the percentiles
            Ybin_low[i] = np.percentile(Y[irange],Percentile_low)
            Ybin_up[i]  = np.percentile(Y[irange],Percentile_up)
            Xbin_low[i] = np.percentile(X[irange],Percentile_low)
            Xbin_up[i]  = np.percentile(X[irange],Percentile_up)
        else:
            Xbin[i] = 0.0
            Ybin[i] = 0.0
            
            # the percentiles
            Ybin_low[i] = 0.0
            Ybin_up[i]  = 0.0
            Xbin_low[i] = 0.0
            Xbin_up[i]  = 0.0

    # remove empty bins
    igood     = np.nonzero(Xbin > 0)
    Xbin      = Xbin[igood]
    Ybin      = Ybin[igood]
    Xbin_low  = Xbin_low[igood]
    Ybin_low  = Ybin_low[igood]
    Xbin_up   = Xbin_up[igood]
    Ybin_up   = Ybin_up[igood]
    
    # eventually plot medians and percentiles
    plt.plot(Xbin,Ybin,linestyle=lstyle,linewidth=lwidth,label=label)
    plt.fill_between(Xbin,Ybin_low,Ybin_up,alpha=0.15)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    return

#==============================================================================
# bin X() data and plot mean(Y()) +- 1-sigma
#==============================================================================
def PlotMean (X, Y, Nbins, sigma, lstyle, lwidth, xlabel, ylabel, label):
      
    # bins the data between min/max
    Xmin  = min(X)
    Xmax  = max(X)
    dX    = (Xmax-Xmin)/(Nbins-1)
    
    # prepare all arrays for the bins
    Xbin     = np.zeros(Nbins)
    Ybin     = np.zeros(Nbins)
    Ybin_low = np.zeros(Nbins)
    Ybin_up  = np.zeros(Nbins)
    Xbin_low = np.zeros(Nbins)
    Xbin_up  = np.zeros(Nbins)
    
    # loop over desired bins
    for i in range(Nbins):
        irange  = np.where( (Xmin+i*dX < X)  &  (X < Xmin+(i+1)*dX) )[0]
        
        if len(irange) > 0:
            Xbin[i] = np.mean(X[irange])
            Ybin[i] = np.mean(Y[irange])
            
            # the percentiles
            Ybin_low[i] = Ybin[i]-sigma*np.std(Y[irange])
            Ybin_up[i]  = Ybin[i]+sigma*np.std(Y[irange])
            Xbin_low[i] = Xbin[i]-sigma*np.std(X[irange])
            Xbin_up[i]  = Xbin[i]+sigma*np.std(X[irange])
        else:
            Xbin[i] = 0.0
            Ybin[i] = 0.0
            
            # the percentiles
            Ybin_low[i] = 0.0
            Ybin_up[i]  = 0.0
            Xbin_low[i] = 0.0
            Xbin_up[i]  = 0.0

    # remove empty bins
    igood     = np.nonzero(Xbin > 0)
    Xbin      = Xbin[igood]
    Ybin      = Ybin[igood]
    Xbin_low  = Xbin_low[igood]
    Ybin_low  = Ybin_low[igood]
    Xbin_up   = Xbin_up[igood]
    Ybin_up   = Ybin_up[igood]
    
    # eventually plot medians and percentiles
    plt.plot(Xbin,Ybin,linestyle=lstyle,linewidth=lwidth,label=label)
    plt.fill_between(Xbin,Ybin_low,Ybin_up,alpha=0.15)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    return

#==============================================================================
# create a contour plot of the scatter relation Y vs X
#==============================================================================
def PlotCountour (X, Y, Levels, Nbins):
  
    # create the 2D sampling points
    x_linear       = np.linspace(min(X),max(X),Nbins)
    y_linear       = np.linspace(min(Y),max(Y),Nbins)
    x_mesh, y_mesh = np.meshgrid(x_linear,y_linear,indexing='ij')  # 'ij' is required to match the rank-ordering as returned by histogram2d()
    
    # create a 2D histogram
    hist_mesh, xedges, yedges = np.histogram2d(X,Y,bins=Nbins,normed=True)
    
    # adjust the levels to the maximum of the histogram
    ScaledLevels = Levels * hist_mesh.max()/100.0
    
    # generate the countours
#    plt.contour( x_mesh,y_mesh,hist_mesh,len(ScaledLevels),levels=ScaledLevels,linestyles='solid',colors='black')
    fig, ax = plt.subplots()
    CS = ax.contour( x_mesh,y_mesh,hist_mesh,len(ScaledLevels),levels=ScaledLevels,linestyles='solid',colors='black')

    fmt  = {}
    strs = []
    for x in Levels:
        strs.append(str(100-x))
    for l, s in zip(CS.levels, strs):
        fmt[l] = s
    
    # Label every other level using strings
    ax.clabel(CS, CS.levels[::1], inline=True, fmt=fmt, fontsize=10)


    # generate the filled area between the contours
    plt.contourf(x_mesh,y_mesh,hist_mesh,len(ScaledLevels),levels=ScaledLevels                   ,cmap='coolwarm')
    
    return


#==============================================================================
# Returns the positions of the values provided in array1[] within the array2[]
#==============================================================================
def Find_arraypositions(array2, array1):
    
	if(np.size(array1)==1):
		positions = np.where(array2 == array1)[0]
	else:    
		sorted_index2    = np.argsort(array2)
		sorted_array2    = array2[sorted_index2]
		sorted_positions = np.searchsorted(sorted_array2, array1)
		positions        = np.take(sorted_index2, sorted_positions, mode='clip')
        
	# tag the positions of array1[] whose values where *not* found in array2[]
	inotfound            = (array2[positions] != array1)
	if(sum(inotfound)>0):
		positions[inotfound] = -1

	return positions


#==============================================================================
# simple way to check for duplicate entries in an array
#==============================================================================
def Check_Duplicates(i):

    n = len(i)
    i = i[np.where(i>0)]
    print('    checking for non-zero duplicates...(original length=',n,', non-zero length=)',len(i))
    
    i.sort()    
    j = []    
    for k in range(1,len(i)):
        if (i[k] == i[k-1]):
            j = j.append(i[k])
            
    if (len(j)>0):
        print(' number of duplicates:',len(j),'; duplicates=',j)
        
    return

#==============================================================================
# simple 2-norm 
#==============================================================================
def norm2(x1,y1,z1,x2,y2,z2):
    
    D = np.sqrt( (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 )
    
    return D

#==============================================================================
# this is how to stack data generated by using data.append(new_data)
#==============================================================================
def Stack_data(data):
	X = [data[i] for i in range(len(data))]
	X = np.array([i for region in X for i in region])
		
	return X

#==============================================================================
# generate a one-to-one line for plots
#==============================================================================
def abline(slope, intercept, **kwargs):
    """Plot a line from slope and intercept"""
    axes = plt.gca()
    x_vals = np.array(axes.get_xlim())
    y_vals = intercept + slope * x_vals
    plt.plot(x_vals, y_vals, **kwargs)
    
#==============================================================================
# down sample given data preserving its distribution function
#==============================================================================
def Downsample_data(data, N):
    
    rand_pos     = np.random.randint(0,len(data),N)
    sorted_index = np.argsort(data)        
    data_sampled = data[sorted_index[rand_pos]]     
    
    return data_sampled

#==============================================================================
# those strings that contain any of the patterns
#==============================================================================
def Extract_strings(strings, patterns):
    
    # this is coded in the most 'un-python' way ever, but c'est la vie...
    Nstrings  = len(strings)
    Npatterns = len(patterns)
        
    use_strings = []
    if(Npatterns > 0):    
        for j in range(Npatterns):
            p = patterns[j]
            
            for i in range(Nstrings):
                s = strings[i]
                
                pos = s.find(p)
                if(pos >= 0):
                    use_strings.append(s)
#                    break
                
    else:
        use_strings = strings
    
#    np.istype(strings,str)
#    np.istype(patterns,str)
    
#    use_strings = [el for el in strings if el in patterns]

    
    return use_strings

#==============================================================================
# shape-mass relation of Tenneti++ (2014, Eq.(A1))    
#==============================================================================
def Tenneti2014(z, gamma, a, M):

    Mpiv = 1e12
    s    = np.zeros(len(M))
    
    for k in range(len(M)):
    
        sum  = 0.0
        for i in range(len(a)):
            sum = sum + a[i]*np.log10(M[k]/Mpiv)**i
        
        s[k] = (1+z)**gamma * sum
    
    return s

#==============================================================================
# shape-mass relation of Allgood++ (2006, Eq.(1))    
#==============================================================================
def Allgood2006(M):

    Ms    = 8e12
    alpha = 0.54
    beta  = -0.05
    
    s = alpha*(M/Ms)**beta

    return s
  
#================================================================================
# Equation (66) as found in Rodriguez-Puebla et al. (2017), corrected for the mistake +1/2
# (Note: this function was originanlly proposed in Behroozi, Conry & Wechsler (2010))
#================================================================================
def RodriguezPuebla2017(Mstar,Mstar0,logM1,beta,delta,gamma):

    Mratio  = Mstar/Mstar0
    logMvir = logM1 + beta*np.log10(Mratio) + (Mratio)**(delta)/(1+(Mratio)**(-gamma)) - 0.5
    
    return 10**logMvir
  
#==============================================================================
# wrapper for scipy function interp1d
#==============================================================================
def interpol_cubic(x, f, xp):

    f = interpolate.interp1d(x, f, kind='cubic')

    return f(xp)

#==============================================================================
# wrapper for scipy function interp1d
#==============================================================================
def interpol_linear(x, f, xp):

    f = interpolate.interp1d(x, f, kind='linear')

    return f(xp)


#==============================================================================
# index search function
#==============================================================================
def find_nearest_idx(array, value):
    array = np.asarray(array)
    idx   = (np.abs(array - value)).argmin()
    return idx

#==============================================================================
# locate jumps in a given function y(x) using its derivative
# (y[] should be a cumulative array, i.e. always increasing!)
#==============================================================================
def Find_jumps(x, y):

    x        = neighboursmooth(x,neighbours=9)

    dx       = np.diff(x)
    dy       = np.diff(y)
    deriv    = dy/dx

    # using negative values in the 2nd derivative
#    xmid     = x[0:len(x)-1]+dx/2
#    ddy      = np.diff(deriv)
#    ddx      = np.diff(xmid)
#    dderiv   = ddy/ddx
#    jump_pos = np.where(dderiv<0)[0]
       
    # as y[] is monotonically increasing, we might compare against the mean slope
    # (appears to be the most stable criterion for the Council of Giants)
    mean_slope = (y[-1]-y[0])/(x[-1]-x[0])
    jump_pos = np.where(deriv>mean_slope)[0]
    
    return jump_pos

#==============================================================================
# smooting function: just average over neighbouring points, no weighting involved
#==============================================================================
def neighboursmooth(x, neighbours=5):
    
    if (neighbours/2 == int(neighbours/2)):
        print('smooth: degree has to be an odd number; returning unsmoothed array')
        return x
    
    N          = len(x)
    x_smoothed = np.zeros(N)
    
    ifirst     = int(neighbours/2+1)-1
    ilast      = N-ifirst-1
    
    x_smoothed[:ifirst] = x[:ifirst]
    x_smoothed[ilast:]  = x[ilast:]

    for j in range(ilast-ifirst+1):
        i = j+ifirst 
        k = i-int(neighbours/2)
        x_smoothed[i] = np.mean(x[k:k+neighbours])
        
    return x_smoothed

#================================================================================
# Luminosity Functions of Pozzetti et al. (2016)
#================================================================================
def LF_Pozzetti2016(L, z, model):

    if (model == 1):
        alpha    = -1.35
        logPhis0 = -2.8
        logLs0   = 41.50
        delta    = 2.0
        eps      = 1.0
        zbreak   = 1.3
        
        Ls       = 10**logLs0 * (1+z)**delta
        if(z<zbreak):
            Phis = 10**logPhis0 * (1+z)**eps
        else:
            Phis = 10**logPhis0 * (1+z)**(-eps) * (1+zbreak)**(2*eps)
        
        # Equation (1) (note, (alpha+1) and *Ls because we want '1/dex' units
        Phi      = Phis * (L/Ls)**(alpha+1) * np.exp(-L/Ls)
        
    elif (model == 2):
        Phi = np.linspace(1.0,1.0,len(L))
        
    elif (model == 3):
        alpha    = -1.587
        logPhis0 = -2.92
        logLs0   = 42.557
        logLs1   = 41.733
        Delta    = 2.288
        beta     = 1.615

        Ls       = 10**(logLs0 + (1.5/(1+z))**beta * (logLs1-logLs0))
        Phis     = 10**(logPhis0)
                
        # Equation (7) (note, (alpha+1) and *Ls because we want '1/dex' units
        Phi      = Phis * (L/Ls)**(alpha+1) / (1+(np.exp(1)-1)*((L/Ls)**(Delta)))
    else:
        Phi = np.linspace(1.0,1.0,len(L))
    
    return Phi
  
#================================================================================
# wrapper for Gaussian Kernel Density calculation
#================================================================================
def Gaussian_KDE(data, covf):

    density = scipy.stats.gaussian_kde(data)
    xs = np.linspace(0, max(data), 200)
    density.covariance_factor = lambda : covf
    density._compute_covariance()

    return xs,density

#================================================================================
# some routines found on the internet that help with fitting an ellipsoid 
#  to a provided point distribution
#================================================================================
def ls_ellipsoid(xx,yy,zz):                                  
    #finds best fit ellipsoid. Found at http://www.juddzone.com/ALGORITHMS/least_squares_3D_ellipsoid.html
    #least squares fit to a 3D-ellipsoid
    #  Ax^2 + By^2 + Cz^2 +  Dxy +  Exz +  Fyz +  Gx +  Hy +  Iz  = 1
    #
    # Note that sometimes it is expressed as a solution to
    #  Ax^2 + By^2 + Cz^2 + 2Dxy + 2Exz + 2Fyz + 2Gx + 2Hy + 2Iz  = 1
    # where the last six terms have a factor of 2 in them
    # This is in anticipation of forming a matrix with the polynomial coefficients.
    # Those terms with factors of 2 are all off diagonal elements.  These contribute
    # two terms when multiplied out (symmetric) so would need to be divided by two
    
    # change xx from vector of length N to Nx1 matrix so we can use hstack
    x = xx[:,np.newaxis]
    y = yy[:,np.newaxis]
    z = zz[:,np.newaxis]
    
    #  Ax^2 + By^2 + Cz^2 +  Dxy +  Exz +  Fyz +  Gx +  Hy +  Iz = 1
    J = np.hstack((x*x,y*y,z*z,x*y,x*z,y*z, x, y, z))
    K = np.ones_like(x) #column of ones
    
    #np.hstack performs a loop over all samples and creates
    #a row in J for each x,y,z sample:
    # J[ix,0] = x[ix]*x[ix]
    # J[ix,1] = y[ix]*y[ix]
    # etc.
    
    JT=J.transpose()
    JTJ = np.dot(JT,J)
    InvJTJ=np.linalg.inv(JTJ);
    ABC= np.dot(InvJTJ, np.dot(JT,K))

    # Rearrange, move the 1 to the other side
    #  Ax^2 + By^2 + Cz^2 +  Dxy +  Exz +  Fyz +  Gx +  Hy +  Iz - 1 = 0
    #    or
    #  Ax^2 + By^2 + Cz^2 +  Dxy +  Exz +  Fyz +  Gx +  Hy +  Iz + J = 0
    #  where J = -1
    eansa=np.append(ABC,-1)

    return (eansa)

def polyToParams3D(vec,printMe):                             
    #gets 3D parameters of an ellipsoid. Found at http://www.juddzone.com/ALGORITHMS/least_squares_3D_ellipsoid.html
    # convert the polynomial form of the 3D-ellipsoid to parameters
    # center, axes, and transformation matrix
    # vec is the vector whose elements are the polynomial
    # coefficients A..J
    # returns (center, axes, rotation matrix)
    
    #Algebraic form: X.T * Amat * X --> polynomial form
    
    if printMe: print('\npolynomial\n',vec)
    
    Amat=np.array(
    [
    [ vec[0],     vec[3]/2.0, vec[4]/2.0, vec[6]/2.0 ],
    [ vec[3]/2.0, vec[1],     vec[5]/2.0, vec[7]/2.0 ],
    [ vec[4]/2.0, vec[5]/2.0, vec[2],     vec[8]/2.0 ],
    [ vec[6]/2.0, vec[7]/2.0, vec[8]/2.0, vec[9]     ]
    ])
    
    if printMe: print('\nAlgebraic form of polynomial\n',Amat)
    
    #See B.Bartoni, Preprint SMU-HEP-10-14 Multi-dimensional Ellipsoidal Fitting
    # equation 20 for the following method for finding the center
    A3=Amat[0:3,0:3]
    
    if(det(A3)>0):    
        A3inv=inv(A3)
        ofs=vec[6:9]/2.0
        center=-np.dot(A3inv,ofs)
        if printMe: print('\nCenter at:',center)
        
        # Center the ellipsoid at the origin
        Tofs=np.eye(4)
        Tofs[3,0:3]=center
        R = np.dot(Tofs,np.dot(Amat,Tofs.T))
        if printMe: print('\nAlgebraic form translated to center\n',R,'\n')
        
        R3=R[0:3,0:3]
        # R3test=R3/R3[0,0]
        # print('normed \n',R3test)
        s1=-R[3, 3]
        R3S=R3/s1
        (el,ec)=eig(R3S)
        
        recip=1.0/np.abs(el)
        axes=np.sqrt(recip)
        if printMe: print('\nAxes are\n',axes  ,'\n')
        
        inve=inv(ec) #inverse is actually the transpose here
        if printMe: print('\nRotation matrix\n',inve)
        
    else:
         center  = [0, 0, 0]
         axes    = [0, 0, 0]
         inve    = 0
         
    return (center,axes,inve)

def printAns3D(center,axes,R,xin,yin,zin,verbose):

      print("\nCenter at  %10.4f,%10.4f,%10.4f" % (center[0],center[1],center[2]))
      print("Axes gains %10.4f,%10.4f,%10.4f " % (axes[0],axes[1],axes[2]))
      print("Rotation Matrix\n%10.5f,%10.5f,%10.5f\n%10.5f,%10.5f,%10.5f\n%10.5f,%10.5f,%10.5f" % (
      R[0,0],R[0,1],R[0,2],R[1,0],R[1,1],R[1,2],R[2,0],R[2,1],R[2,2]))


      # Check solution
      # Convert to unit sphere centered at origin
      #  1) Subtract off center
      #  2) Rotate points so bulges are aligned with axes (no xy,xz,yz terms)
      #  3) Scale the points by the inverse of the axes gains
      #  4) Back rotate
      # Rotations and gains are collected into single transformation matrix M

      # subtract the offset so ellipsoid is centered at origin
      xc=xin-center[0]
      yc=yin-center[1]
      zc=zin-center[2]

      # create transformation matrix
      L = np.diag([1/axes[0],1/axes[1],1/axes[2]])
      M=np.dot(R.T,np.dot(L,R))
      print('\nTransformation Matrix\n',M)

      # apply the transformation matrix
      [xm,ym,zm]=np.dot(M,[xc,yc,zc])
      # Calculate distance from origin for each point (ideal = 1.0)
      rm = np.sqrt(xm*xm + ym*ym + zm*zm)

      print("\nAverage Radius  %10.4f (truth is 1.0)" % (np.mean(rm)))
      print("Stdev of Radius %10.4f\n " % (np.std(rm)))

      return

# this is a wrapper routine for
#    a) find the points that define the convex hull (if wanted)
#    b) use either the convex hull or the full 3D points to fit an ellipsoid
#    c) return the ellipsoid parameters
def fit_ellipsoid(x,y,z,use_convhull='yes',plot_data='no'):
    
    if(plot_data=='yes'):
        fig = plt.figure(99)
        ax  = fig.add_subplot(111, projection='3d')
        
        minx = min(x)
        miny = min(y)
        minz = min(z)
        maxx = max(x)
        maxy = max(y)
        maxz = max(z)
        minxyz = min([minx, miny, minz])
        maxxyz = max([maxx, maxy, maxz])
        
        ax.scatter(x, y, z)
        ax.set_xlim([minxyz, maxxyz])
        ax.set_ylim([minxyz, maxxyz])
        ax.set_zlim([minxyz, maxxyz])
    
    if(use_convhull=='yes'):
        #get convex hull
        surface  = np.stack((x,y,z), axis=-1)
        hullV    = ConvexHull(surface)
        lH       = len(hullV.vertices)
        hull     = np.zeros((lH,3))
        for i in range(len(hullV.vertices)):
            hull[i] = surface[hullV.vertices[i]]
        hull     = np.transpose(hull)        
             
        if(plot_data=='yes'):
            fig = plt.figure(98)
            ax  = fig.add_subplot(111, projection='3d')
            
            ax.scatter(hull[0], hull[1], hull[2])
            ax.set_xlim([minxyz, maxxyz])
            ax.set_ylim([minxyz, maxxyz])
            ax.set_zlim([minxyz, maxxyz])
        
               
        #fit ellipsoid using convex hull
        eansa = ls_ellipsoid(hull[0],hull[1],hull[2]) #get ellipsoid from convex hull
    else:
        #fit ellipsoid using all points
        eansa = ls_ellipsoid(x,y,z) #get ellipsoid from 3D points
        
    center,axes,inve = polyToParams3D(eansa,False)   #get ellipsoid 3D parameters
    
    # pack up returndata
    returndata = {'eansa':          eansa,
                  'center':         center,
                  'axes':           axes,
                  'rotationMatrix': inve}
    
    return(returndata)
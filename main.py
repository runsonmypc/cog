#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 12:39:37 2018

@author: Alexander Knebe
"""

#==============================================================================
# import all relevant modules
#==============================================================================

# MyLibrary.py and ahf.py are stored globally on my laptop
import sys
#sys.path.append("/Users/aknebe/Office/Source/Python/")

import numpy             as np
import MyLibrary         as MyL
import CouncilOfGiants   as CoG
import ahf               as ahf
import matplotlib.pyplot as plt

from skspatial.objects import Points, Plane
from skspatial.plotting import plot_3d

#==============================================================================
# all relevant constants
#==============================================================================
Cdef = CoG.def_constants()
C    = np.zeros(1,dtype=Cdef)

C['verbosity'] = 1  # 0=silence, 1=sign-posting, 2=reading 3=selection/calculation details

# SIM files tell which of the simulations contains credible Virgo cluster (VG) and additionally credible Local Groups (LG)
#C['SIMfile']         = ''  # this would use all 795 simulations available
#C['SIMfile']         = 'CGs_512_DM_ONLY-VG_full.txt'
#C['SIMfile']         = 'CGs_512_DM_ONLY-VG_LSS.txt'
#C['SIMfile']         = 'CGs_512_DM_ONLY-VG_53.txt'
C['SIMfile']         = 'CGs_512_DM_ONLY-LG.txt'

# which peak to consider?
peakid                     = 1

# plot parameters
C['d_virgo_normalisation'] = 1     # 0:no / 1:yes
C['Npeaks']                = 2     # how many peaks to consider
C['f_obs']                 = 0.8   # we allow for 80% of observational values when considering lower limits (e.g. Mhalo, Nobjects, etc.)
C['f_width']               = 1.05  # making the peak width wider when extracting objects
C['f_height']              = 0.50  # only consider simulation peaks with height>frac*obs_height
C['LowerPercentile']       = 25    # percentiles
C['Nbins']                 = 50    # number of bins
C['Rsphere']               = 12.0  # Select_halos() out to this distance from (50/50/50) to start with
C['MhaloLimit_obs']        = 6.74e11 # min. halo mass (M200c) in observed council (min(M*_Council)=10.302, IC 342)


# observational data
C['obsfile']               = 'D:/cog/Data/council_data-M200c.txt'
C['d_virgo_observed']      = 16.5 # Mpc
C['RLG']                   = C['Rsphere'] # Mpc

# simulation details
C['hubble']           = 0.6777
C['BoxSize']          = 100.0/C['hubble'][0] # Mpc
C['read_ascii_halos'] = 0  # 0:no / 1:yes
C['save_as_npy']      = 0  # 0:no / 1:yes
C['datapath']         = 'D:/cog/Data/CouncilOfGiants/'
C['PDFdir']           = 'PDFs/'



if(C['d_virgo_normalisation'][0]==0):
    C['d_virgo_observed']= 1.0  # Mpc


###############################################################################
# LOAD IN DATA
###############################################################################
#==============================================================================
#   scanning 'datapath' for available regions
#==============================================================================
# scan the provided datapath for all possible files
#  -> those filenames will be passed to the respective analysis routine and read there
#     (note, for memory reasons not all data will be loaded at once...)
halosfiles = CoG.Scan_datapath(C['datapath'][0], C['read_ascii_halos'][0])

#==============================================================================
#   restrict to what data should be looked for/worked with?
#   here we also set the centre of the coordinate system for both sim and obs
#==============================================================================
SIMs, LGcentre, d_virgo = CoG.Read_SIMfile(C)
halosfiles = MyL.Extract_strings(halosfiles, SIMs)

#==============================================================================
#   load in all data:
#       simulations into one single array halos[isimu]['AHFcolumn'][ihalo]
#       obsverations into obsdata
#==============================================================================
halos   = CoG.Read_halos(halosfiles, C) # only reads selected properties and converts to Mpc and Msun (without h!)
obsdata = CoG.Read_observations(C)



###############################################################################
# now for the analysis of interest...
###############################################################################

# 1) KDE - for each simulation, it will provide:
#      peakstats['peak_pos']
#      peakstats['peak_height']
#      peakstats['peak_width']
#      peakstats['simuid']
#      (where the first entry [0] is for the observational data w/ simuid=-1)
#------------------------------------------------------------------------------
peakstats = CoG.KDE(halos, LGcentre, d_virgo, C, plot='yes')


# 2) check the statistics of peak height = criterion to remove peaks  
#------------------------------------------------------------------------------
CoG.Plot_peakheight(peakstats, C)

# 3) Remove_peaks - remove simulation peaks that are not considered 'significant'
peakstats = CoG.Remove_peaks(peakstats, C)

# check the statistics of width and position in comparison to observations   
#------------------------------------------------------------------------------
CoG.Plot_peakpos(peakstats, C)
CoG.Plot_peakwidth(peakstats, C)


# 4) Find_peakhalos - for a specified peak (peakid=[0,1,2]) we extract the following information of halos that lie within peak_pos+-peak_width:
#  peakhalodata['peaksimuid']   :  the simulation the halos belong to
#  peakhalodata['peakhaloids']  :  just the 'haloid' of those halos
#  peakhalodata['peakhalos']    :  the full halo catalogue in AHF format
#  peakhalodata['peakhalodist'] :  the distance of those haloes to the LG centre (in units of d_virgo!)
# Find_peakobs - also extract the corresponding observerd galaxies
#------------------------------------------------------------------------------
peakhalodata = CoG.Extract_peakhalos(halos, peakstats, peakid, LGcentre, d_virgo, C)
peakobsdata  = CoG.Extract_peakobs(obsdata, peakstats, peakid, C)


# 5) ...and now use the peak halos and peak galaxies:
#------------------------------------------------------------------------------
# number of objects in peak
numbers = CoG.Plot_numbers(peakhalodata, peakobsdata, peakid, C)
Nobs = numbers['Nobs'][0]
print('number of objects in observed council:',Nobs)

# masses of objects in peak
masses  = CoG.Plot_masses(peakhalodata, peakobsdata, peakid, C)

# best-fit plane for objects in peak
chiplane  = CoG.Plot_planefit(peakhalodata, peakobsdata, peakid, LGcentre, C)

simuid  = chiplane['simuid']
#simutag = CoG.Extract_simutag(halosfiles[simuid])
#print('simulations matching observations:',simutag)

#Npeaksimus = len(peakhalodata['peaksimuid'])
#simuid     = 0
#peakhalos = peakhalodata['peakhalos'][simuid]
#CoG.Plot_3D(peakhalos, C)

# Center of mass plots
#MyL.PlotCumulative(peakhalodata['peakcofmdist'], 1, 100, 'o', 0.5, "simulations", "C of M dist", "plot")
plt.clf()
a_one = np.ones(len(peakhalodata['peakcofmdist']))
plt.plot(np.sort(peakhalodata['peakcofmdist']), np.cumsum(a_one), label='simulations')
plt.axvline(peakobsdata['peakcofmdist'], color='r', label='observation')
plt.xlabel("Distance, $Mpc$")
plt.ylabel("Number of simulations")
plt.title("Cumulative distribution of center of mass distances")
plt.legend()
plt.show()

#==============================================================================
# testing.... (those routines are in CoG-pool.py and need to be copied before using)
#==============================================================================
# ca      = CoG.Plot_ca(peakhalodata, peakobsdata, peakid, LGcentre, C)

# this simply plots the distributions for the mass and distance of the simulated Virgo clusters
#CoG.Plot_virgo(d_virgo, m_virgo, C)

#CoG.Plot_cHMF(halosfiles, C)

#CoG.Plot_MassDistance(halosfiles, C)

#CoG.Plot_cDist(halosfiles, C)

#CoG.Plot_JS(halosfiles, C)

#CoG.Plot_3D(halosfiles, C)

#------------------------------------------------------------------------------
# check that there are no duplicates in strings
#string = SIMs
#string = halosfiles
#for i,s1 in enumerate(string):
#    for j,s2 in enumerate(string):
#        if(j > i):
#            pos = s1.find(s2)
#            if(pos>=0):
#                print(i)
#                print(s1)
#                print(s2)
#                print('')
#------------------------------------------------------------------------------




#------------------------------------------------------------------------------
#halos = ahf.Read_AHF_DMonly(halosfiles[0], C['read_ascii_halos'][0], C['save_as_npy'][0], fields=['haloid', 'Mhalo', 'Xhalo', 'Yhalo', 'Zhalo'])
#
#virgo_pos = np.where(halos['haloid'] == 127000000000012)[0]
#X = 50000
#Y = 50000
#Z = 50000
#
#dx = (halos['Xhalo'][virgo_pos]-X)
#dy = (halos['Yhalo'][virgo_pos]-Y)
#dz = (halos['Zhalo'][virgo_pos]-Z)
#
#dist = np.sqrt(dx**2+dy**2+dz**2)/C['hubble']
#print(dist)
#
#print(dx/C['hubble'],dy/C['hubble'],dz/C['hubble'],halos['Mhalo'][virgo_pos]/C['hubble'])
#print((halos['Xhalo'][virgo_pos])/C['hubble'],(halos['Yhalo'][virgo_pos])/C['hubble'],(halos['Zhalo'][virgo_pos])/C['hubble'],halos['Mhalo'][virgo_pos]/C['hubble'])
#------------------------------------------------------------------------------



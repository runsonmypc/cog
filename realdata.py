import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import MyLibrary as MyL
from halo2stellar import s2h, xdata, ydata
import ahf

xyz = [[-1.756, -1.067, 0.321], [0.296, -0.701, 0.288], [-2.42, -2.976, 0.32], [-2.382, -2.506, 0.243],
[-1.687, -1.162, 0.059], [0.207, -0.863, 0.134], [-0.16, -8.936, -0.339], [1.094, -5.666, -0.202],
[4.592, -8.931, -0.971], [3.081, -8.899, -1.513], [4.558, -9.665, -1.86], [2.29, -2.352, 0.0],
[2.406, -2.452, -0.044], [2.572, -2.604, -0.185], [-4.019, -0.268, -1.376], [2.758, -1.904, -0.141],
[2.264, -1.521, -0.353], [3.116, -0.838, -0.742], [5.672, 1.022, -4.938], [-1.339, 5.356, -8.696],
[7.524, -0.891, -1.021], [5.492, 3.17, -6.038], [3.709, -0.262, -0.318], [3.45, -0.266, -0.257],
[1.93, 6.678, -7.471], [9.407, 7.2, -6.318], [5.368, 6.791, -5.644], [5.283, 6.717, -5.424],
[5.901, 6.908, -5.562], [5.44, 6.743, -5.39], [5.868, 7.261, -5.795], [6.113, 7.4, -5.761],
[6.619, 8.112, -5.762], [-1.628, 6.066, -3.368], [5.593, 7.539, -4.224], [6.739, 3.011, -0.395],
[4.393, 0.182, 0.412], [3.626, 2.335, -0.266], [6.953, 3.054, -0.182], [3.609, 1.858, 0.013],
[1.161, 9.126, -1.491], [5.833, 4.742, -0.082], [3.862, 2.325, 0.27], [3.179, 3.842, 0.086],
[-1.72, 2.814, -0.351], [5.752, 3.156, 0.832], [-0.312, 5.202, -0.058], [-1.0, 2.91, -0.075],
[-1.518, 3.197, -0.125], [7.129, 3.455, 1.451], [6.746, 3.259, 1.383], [-1.096, 4.813, 0.168],
[6.811, 2.278, 2.009], [-3.193, 2.809, -0.038], [-1.757, 2.502, 0.829], [-0.004, 0.004, 0.135],
[4.5, -0.765, 2.513], [4.269, -2.466, 3.817], [-5.736, 0.9, 1.128], [-2.995, -2.226, 0.827]]

xyz = np.array(xyz)

xs = xyz[:, 0] - 0.124
ys = xyz[:, 1] + 0.297
zs = xyz[:, 2] - 0.2
data_r = np.sqrt(xs**2 + ys**2 + zs**2)
data_xyz = zip(xs, ys, zs)

m = [9.445, 11.042, 9.648, 10.805, 9.468, 9.636, 10.196, 9.14, 10.856, 9.819, 10.935, 10.928, 10.493,
9.773, 9.511, 10.302, 8.246, 9.791, 10.521, 10.877, 10.224, 10.586, 10.905, 10.573, 11.016, 10.356,
10.589, 10.717, 10.355, 10.862, 10.712, 10.344, 10.468, 10.093, 10.809, 8.991, 9.182, 9.410, 10.701,
9.28, 11.303, 10.19, 10.458, 10.496, 10.528, 8.736, 9.501, 9.37, 11.169, 10.647, 10.524, 10.642,
10.494, 10.559, 9.235, 11.095, 9.635, 10.529, 9.366, 9.448]

m = np.array(m)
data_mhalo = s2h(m)


def make_csv():
    from halo2stellar import s2h

    hm = data_mhalo

    import csv

    with open('council_data.csv', mode='w') as csvfile:
        header = ["log of stellar mass", "log of halo mass",
                  "x (sheet co-ordinates)", "y (sheet co-ordinates)",
                  "z (sheet co-ordinates)"]
        # , quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer = csv.writer(csvfile)
        writer.writerow(header)
        for i in range(len(m)):
            row = m[i], hm[i], xyz[i][0], xyz[i][1], xyz[i][2]
            writer.writerow(row)


def Plot3D():
    
    fig = plt.figure()

    xlabel = '$x$'
    ylabel = '$y$'
    zlabel = '$z$'

    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(xs, ys, zs, s = (m - 8)**4 + 10)

    # reference circle/disk/sphere stuff
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    cx = 3.75 * np.sin(u)
    cy = 3.75 * np.cos(u)
    ax.plot(cx, cy, color='g')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)

    plt.show()


def Plot_s2h():
    
    plt.plot(xdata, ydata, label="SMHM Rodriguez Puebla 2017", zorder=0)
    plt.scatter(m, data_mhalo, marker='o', color='r', s=30, alpha=0.7,
            label="McCall 2014", zorder=1)

    plt.ylabel('$\log_{10} (M_{halo} [M_{\odot}])$')
    plt.xlabel('$\log_{10} (M_{stellar} [M_{\odot}])$')
    plt.legend()
    plt.savefig('PDFs/mass_conversion.pdf')
    plt.show()


def mrd_test_plot(xyz, m):
    "Plots log mass on y-axis and radial distance on x-axis"
    r = np.sqrt(xyz[:,0]**2 + xyz[:,1]**2 + xyz[:,2]**2)
    print(r)
    plt.scatter(r, m)
    plt.xlabel("log of radial distance (Mpc)")
    plt.ylabel("log of stellar mass")
    plt.show()


#mrd_test_plot(xyz, data_mhalo)
#Plot_s2h()
make_csv()

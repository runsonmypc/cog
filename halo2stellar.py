import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import stats
import numpy as np
from scipy import interpolate
import sys, os


def lin(x, a, b):
    "basic linear function"
    return a * x + b

def poly3(x, a, b, c, d):
    "polynomial function to the third power"
    return a * x**3 + b * x**2 + c * x + d

def interpol(x, f, xp):
    fp = interpolate.interp1d(x, f, kind='cubic')
    return fp(xp)


data_path = os.path.abspath(os.path.dirname(__file__))
datafile = os.path.join(data_path, 'Data/SMHM-Rodriguez-Puebla2017.dat')
data = np.loadtxt(datafile)

xdata = data[:,0]
ydata = data[:,1]

tdata = np.empty((0,3))
for i in range(len(data)):
    if data[:,0][i] >= 10:
        tdata = np.vstack([tdata, data[i]])

xtdata = tdata[:,0]
ytdata = tdata[:,1]

def conversion_plot():
    
    plt.plot(xdata, ydata, 'b-', label='Puebla 2017')

    # Fit for the parameters a, b of lin function:
    slope, intercept, r_val, p_val, std_err = stats.linregress(xtdata, ytdata)
    plt.plot(xtdata, lin(xtdata, slope, intercept), 'r--', label='linear fit')

    # Fit for the parameters a, b of poly3 function:
    poly = np.polyfit(xdata, ydata, 3)
    plt.plot(xdata, poly3(xdata, poly[0], poly[1], poly[2], poly[3]),
    'g--', label='poly fit')

    # Interpolation:
    ys = interpol(xdata, ydata, np.linspace(min(xdata), max(xdata), 1000))
    plt.plot(np.linspace(min(xdata), max(xdata), 1000), ys, 'c--',
    label='interpolation')

    plt.ylabel('log of Halo Mvir')
    plt.xlabel('log of stellar mass')
    plt.legend()
    plt.show()


#·············································································#

def s2h(stellarmass):
    "convert list of stellar masses to dark matter halo mass"
    h = interpol(xdata, ydata, stellarmass)
    H = pow(10, h) / 1.15
    return np.log10(H)

